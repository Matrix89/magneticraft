package com.cout970.magneticraft.parts.electric;

import com.cout970.magneticraft.api.electricity.ElectricConstants;
import com.cout970.magneticraft.api.electricity.IElectricConductor;
import com.cout970.magneticraft.api.electricity.IElectricTile;
import com.cout970.magneticraft.api.electricity.prefab.ElectricConductor;
import com.cout970.magneticraft.api.util.MgDirection;
import com.cout970.magneticraft.api.util.VecInt;
import net.minecraft.tileentity.TileEntity;

/**
 * Created by M89 on 1/4/2016.
 * m89.quizzor.pl
 */
public class CableBase extends TileEntity implements IElectricTile {
    private int ImmibisMicroblocks_TransformableTileEntityMarker;
    IElectricConductor conductor = new ElectricConductor(this, 0, ElectricConstants.RESISTANCE_COPPER_LOW);

    @Override
    public IElectricConductor[] getConds(VecInt dir, int Vtier) {
        MgDirection mgdir = dir.toMgDirection();
        if(Vtier == conductor.getTier() && mgdir != null && ImmibisMicroblocks_isSideOpen(mgdir.toForgeDir().ordinal()))
            return new IElectricConductor[]{conductor};
        return null;
    }

    @Override
    public void updateEntity() {
        super.updateEntity();
        conductor.recache();
        conductor.iterate();
    }

    public boolean ImmibisMicroblocks_isSideOpen(int side) {
        return true;
    }

    public void ImmibisMicroblocks_onMicroblocksChanged() {

    }

}
