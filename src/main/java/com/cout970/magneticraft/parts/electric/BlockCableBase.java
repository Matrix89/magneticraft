package com.cout970.magneticraft.parts.electric;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Created by M89 on 1/4/2016.
 * m89.quizzor.pl
 */
public class BlockCableBase extends BlockContainer {
    private int ImmibisMicroblocks_TransformableBlockMarker;
    public static int renderId = RenderingRegistry.getNextAvailableRenderId();
    public BlockCableBase() {
        super(Material.iron);
        setBlockName("Cable");
        //TODO setBlockTextureName("");
        GameRegistry.registerBlock(this,"CableBase");
        GameRegistry.registerTileEntity(CableBase.class,"TECableBase");
        setCreativeTab(CreativeTabs.tabRedstone);
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new CableBase();
    }

    @Override
    public int getRenderType() {
        return renderId;
    }

    @Override
    public boolean isNormalCube() {
        return false;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
