package com.cout970.magneticraft.parts.electric.wires;

import com.cout970.magneticraft.parts.electric.BlockCableBase;
import com.cout970.magneticraft.parts.electric.CableBase;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;

/**
 * Created by M89 on 1/4/2016.
 * m89.quizzor.pl
 */
public class TERCableLow implements ISimpleBlockRenderingHandler {
    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {

    }


    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        TileEntity te = world.getTileEntity(x, y, z);
        if (!(te instanceof CableBase)) {
            return false;
        }
        Tessellator.instance.addTranslation(x, y, z);
        Tessellator.instance.setBrightness(15);

        Tessellator.instance.setColorRGBA(65, 65, 65, 255);
        float low = 0.4f;
        float height = 0.6f;
        float u = block.getIcon(0,0).getMinU();
        float v = block.getIcon(0,0).getMaxV();
        if (!((CableBase) te).ImmibisMicroblocks_isSideOpen(ForgeDirection.NORTH.ordinal())) {
            Tessellator.instance.setNormal(1,0,0);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.setNormal(0,0,0);
        } else {
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, 0, u, v);
            Tessellator.instance.addVertexWithUV(low, height, 0, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);

            Tessellator.instance.addVertexWithUV(height, low, 0, u, v);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.addVertexWithUV(low, low, 0, u, v);

            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, 0, u, v);
            Tessellator.instance.addVertexWithUV(low, low, 0, u, v);

            Tessellator.instance.addVertexWithUV(height, low, 0, u, v);
            Tessellator.instance.addVertexWithUV(height, height, 0, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
        }

        if (!((CableBase) te).ImmibisMicroblocks_isSideOpen(ForgeDirection.WEST.ordinal())) {
            Tessellator.instance.setNormal(0,0,1);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.setNormal(0,0,0);
        } else {
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(0, height, low, u, v);
            Tessellator.instance.addVertexWithUV(0, height, height, u, v);

            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.addVertexWithUV(0, low, height, u, v);
            Tessellator.instance.addVertexWithUV(0, low, low, u, v);

            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(0, height, height, u, v);
            Tessellator.instance.addVertexWithUV(0, low, height, u, v);

            Tessellator.instance.addVertexWithUV(0, low, low, u, v);
            Tessellator.instance.addVertexWithUV(0, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
        }

        if (!((CableBase) te).ImmibisMicroblocks_isSideOpen(ForgeDirection.SOUTH.ordinal())) {
            Tessellator.instance.setNormal(1,0,0);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.setNormal(0,0,0);
        } else {
            Tessellator.instance.addVertexWithUV(height, height, 1, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, 1, u, v);

            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(height, low, 1, u, v);
            Tessellator.instance.addVertexWithUV(low, low, 1, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);

            Tessellator.instance.addVertexWithUV(low, low, 1, u, v);
            Tessellator.instance.addVertexWithUV(low, height, 1, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);

            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(height, height, 1, u, v);
            Tessellator.instance.addVertexWithUV(height, low, 1, u, v);
        }

        if (!((CableBase) te).ImmibisMicroblocks_isSideOpen(ForgeDirection.EAST.ordinal())) {
            Tessellator.instance.setNormal(0,0,1);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.setNormal(0,0,0);
        } else {
            Tessellator.instance.addVertexWithUV(1, height, height, u, v);
            Tessellator.instance.addVertexWithUV(1, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);

            Tessellator.instance.addVertexWithUV(1, low, low, u, v);
            Tessellator.instance.addVertexWithUV(1, low, height, u, v);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);

            Tessellator.instance.addVertexWithUV(1, low, height, u, v);
            Tessellator.instance.addVertexWithUV(1, height, height, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);

            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(1, height, low, u, v);
            Tessellator.instance.addVertexWithUV(1, low, low, u, v);
        }

        if (!((CableBase) te).ImmibisMicroblocks_isSideOpen(ForgeDirection.UP.ordinal())) {
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
        } else {
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);
            Tessellator.instance.addVertexWithUV(low, 1, low, u, v);
            Tessellator.instance.addVertexWithUV(height, 1, low, u, v);
            Tessellator.instance.addVertexWithUV(height, height, low, u, v);

            Tessellator.instance.addVertexWithUV(low, height, height, u, v);
            Tessellator.instance.addVertexWithUV(low, 1, height, u, v);
            Tessellator.instance.addVertexWithUV(low, 1, low, u, v);
            Tessellator.instance.addVertexWithUV(low, height, low, u, v);

            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
            Tessellator.instance.addVertexWithUV(height, 1, height, u, v);
            Tessellator.instance.addVertexWithUV(low, 1, height, u, v);
            Tessellator.instance.addVertexWithUV(low, height, height, u, v);

            Tessellator.instance.addVertexWithUV(height, height, low, u, v);
            Tessellator.instance.addVertexWithUV(height, 1, low, u, v);
            Tessellator.instance.addVertexWithUV(height, 1, height, u, v);
            Tessellator.instance.addVertexWithUV(height, height, height, u, v);
        }

        if (!((CableBase) te).ImmibisMicroblocks_isSideOpen(ForgeDirection.DOWN.ordinal())) {
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
        } else {
            Tessellator.instance.addVertexWithUV(low, 0, low, u, v);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.addVertexWithUV(height, 0, low, u, v);

            Tessellator.instance.addVertexWithUV(low, 0, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, low, u, v);
            Tessellator.instance.addVertexWithUV(low, 0, low, u, v);

            Tessellator.instance.addVertexWithUV(height, 0, height, u, v);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, low, height, u, v);
            Tessellator.instance.addVertexWithUV(low, 0, height, u, v);

            Tessellator.instance.addVertexWithUV(height, 0, low, u, v);
            Tessellator.instance.addVertexWithUV(height, low, low, u, v);
            Tessellator.instance.addVertexWithUV(height, low, height, u, v);
            Tessellator.instance.addVertexWithUV(height, 0, height, u, v);
        }

        Tessellator.instance.addTranslation(-x, -y, -(z));
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return false;
    }

    @Override
    public int getRenderId() {
        return BlockCableBase.renderId;
    }
}
